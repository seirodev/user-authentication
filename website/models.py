from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.forms.widgets import FileInput
from PIL import Image
from django.utils.text import slugify
from django.urls import reverse

# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(
        default='placeholder.png', upload_to='profile_pics',
        null=True, 
        blank=True,
    )
    bio = models.TextField(max_length=500, blank=True)
    website = models.CharField(max_length=30, blank=True)
    location = models.CharField(max_length=30, blank=True)
    instagram_handle = models.CharField(max_length=30, blank=True)

    def __str__(self, *args, **kwargs):
        return f'{self.user.username} Profile'

    def save(self):
        super().save(force_insert=False)

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)

    @receiver(post_save, sender=User)
    def create_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)
    post_save.connect(create_profile, sender=User)
