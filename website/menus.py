from django.conf import settings
from django.urls import reverse
from menu import Menu, MenuItem

# When Logged In
Menu.add_item('logged_in', MenuItem('Logout', reverse('logout')))
Menu.add_item('logged_in', MenuItem('Profile', reverse('profile')))
Menu.add_item('logged_in', MenuItem('Settings', reverse('settings')))
# Menu.add_item('logged_in', MenuItem('Change Password', reverse('change-password')))

# When Logged Out
Menu.add_item('logged_out', MenuItem('Login', reverse('login')))
Menu.add_item('logged_out', MenuItem('Register', reverse('register')))