from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordChangeForm
from django.contrib.auth.models import User
from django.contrib import messages
from django.utils.text import slugify
from .forms import SignUpForm, EditSettingsForm, ChangePasswordForm, ProfileForm
from .models import Profile

# Create your views here.
def home(request):
    users = User.objects.all()
    context = {
        'users': users,
    }
    return render(request, 'website/home.html', context)

def login_user(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, 'You have successfully signed in!', extra_tags='is-success')
            return redirect('home')
        else: 
            messages.error(request, 'Error logging in, try again', extra_tags='is-danger')
            return redirect('login')
    else:   
        return render(request, 'website/login.html', {})

def logout_user(request):
    logout(request)
    messages.success(request, 'You have been logged out', extra_tags='is-success')
    return redirect('login')

def register_user(request, *args, **kwargs):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save(*args, **kwargs)
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            Profile.create_profile(User, user)
            login(request, user)
            messages.success(request, 'You have successfully registered!', extra_tags='is-success')
            return redirect('home')
        else:
            messages.error(request, 'You have errors', extra_tags="is-warning")
    else:
        form = SignUpForm()
    context = {
        'form': form
    }
    return render(request, 'website/register.html', context)

def settings(request):
    if request.method == 'POST':
        form = EditSettingsForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            messages.success(request, 'Your changes were saved!', extra_tags='is-success')
            return redirect('home')
        else:
            messages.error(request, 'You have errors', extra_tags="is-warning")
    else:
        form = EditSettingsForm(instance=request.user)
    context = {
        'form': form
    }
    return render(request, 'website/settings.html', context)

def change_password(request):
    if request.method == 'POST':
        form = ChangePasswordForm(data=request.POST, user=request.user)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            messages.success(request, 'Your password was changed!', extra_tags='is-success')
            return redirect('home')
        else:
            messages.error(request, 'You have errors', extra_tags="is-warning")
    else:
        form = ChangePasswordForm(user=request.user)
    context = {
        'form': form
    }
    return render(request, 'website/change-password.html', context)

def profile(request):
    username = request.user.username
    instance = get_object_or_404(User, username=username)
    context = {
        'instance': instance,
    }
    return render(request, 'website/profile.html', context)

def edit_profile(request):
    if request.method == 'POST':
        form = ProfileForm(request.POST,
                           request.FILES, 
                           instance=request.user.profile)
        if form.is_valid():
            form.save()
            messages.success(request, 'Your changes were saved!', extra_tags='is-success')
        else:
            messages.error(request, 'You have errors', extra_tags="is-warning")
    else:
        form = ProfileForm(instance=request.user.profile)
    context = {
        'form': form
    }
    return render(request, 'website/edit-profile.html', context)