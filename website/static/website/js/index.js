document.addEventListener('DOMContentLoaded', () => {
const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
    if ($navbarBurgers.length > 0) {
        $navbarBurgers.forEach( el => {
            el.addEventListener('click', () => {
                const target = el.dataset.target;
                const $target = document.getElementById(target);
                el.classList.toggle('is-active');
                $target.classList.toggle('is-active');
            });
        });
    }
});

notificationMessage = document.querySelector('.message-info');

function dismiss() {
    notificationMessage.classList.add('is-hidden');
}

// navbarLink = document.querySelector('.navbar-link.is-arrowless');
// navbarDropdownLink = document.querySelector('.navbar-item.has-dropdown');

// navbarLink.addEventListener('click', function() {
//     navbarDropdownLink.classList.toggle('is-active');
// })