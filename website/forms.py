from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordChangeForm
from django.contrib.auth.models import User
from django import forms
from .models import Profile
from django.forms.widgets import FileInput

class SignUpForm(UserCreationForm):
    email = forms.EmailField(label='', widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'Email Address'}))
    first_name = forms.CharField(label='', max_length=100, widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'First Name'}))
    last_name = forms.CharField(label='', max_length=100, widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'Last Name'}))
    class Meta:
        model = User
        fields = (
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2',
        )
    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)

        self.fields['username'].widget.attrs['class'] = 'input'
        self.fields['password1'].widget.attrs['class'] = 'input'
        self.fields['password2'].widget.attrs['class'] = 'input'

        self.fields['username'].help_text = ''
        self.fields['password1'].help_text = ''
        self.fields['password2'].help_text = ''

        self.fields['username'].label = ''
        self.fields['password1'].label = ''
        self.fields['password2'].label = ''

        self.fields['username'].widget.attrs['placeholder'] = 'User Name'
        self.fields['password1'].widget.attrs['placeholder'] = 'Password'
        self.fields['password2'].widget.attrs['placeholder'] = 'Confirm Password'

class EditSettingsForm(UserChangeForm):
    email = forms.EmailField(label='', widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'Change Email Address'}))
    first_name = forms.CharField(label='', max_length=100, widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'Change First Name'}))
    last_name = forms.CharField(label='', max_length=100, widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'Change Last Name'}))
    password = forms.CharField(label='', widget=forms.TextInput(attrs={'type': 'hidden'}))
    class Meta:
        model = User
        fields = (
            'username',
            'first_name',
            'last_name',
            'email',
            'password',
        )
    def __init__(self, *args, **kwargs):
        super(EditSettingsForm, self).__init__(*args, **kwargs)

        self.fields['username'].widget.attrs['class'] = 'input'
        self.fields['username'].help_text = ''
        self.fields['username'].label = ''
        self.fields['username'].widget.attrs['placeholder'] = 'Change User Name'
        self.fields['password'].label = ''

class ChangePasswordForm(PasswordChangeForm):
    old_password = forms.CharField(label='', widget=forms.PasswordInput(attrs={'class': 'input', 'placeholder': 'Old Password'}))
    new_password1 = forms.CharField(label='', widget=forms.PasswordInput(attrs={'class': 'input', 'placeholder': 'New Password'}))
    new_password2 = forms.CharField(label='', widget=forms.PasswordInput(attrs={'class': 'input', 'placeholder': 'Confirm Password'}))
    class Meta:
        model = User
        fields = (
            'old_password',
            'new_password1',
            'new_password2',
        )

class ProfileForm(forms.ModelForm):
    # image = forms.FileField(widget=FileInput)
    class Meta:
        model = Profile
        fields = (
            'image',
            'bio',
            'website',
            'location',
            'instagram_handle'
        )